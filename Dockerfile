FROM ubuntu:latest

MAINTAINER Meunier laurent <laurentmg@magelo.com>

# Elixir requires UTF-8
RUN apt-get update && apt-get upgrade -y && apt-get -y install locales && locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8
ENV TZ Europe/Paris

# update and install software
RUN apt-get install -y curl wget make sudo inotify-tools bzip2 git \
    # download and install Erlang apt repo package
    && wget http://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb \
    && dpkg -i erlang-solutions_1.0_all.deb \
    && apt-get update \
    && rm erlang-solutions_1.0_all.deb \
    # For some reason, installing Elixir tries to remove this file
    # and if it doesn't exist, Elixir won't install. So, we create it.
    # Thanks Daniel Berkompas for this tip.
    # http://blog.danielberkompas.com
    && touch /etc/init.d/couchdb \
    # install latest elixir package
    && apt-get install -y elixir erlang-dev erlang-dialyzer erlang-parsetools \
    # clean up after ourselves
    && apt-get clean

# install the Phoenix Mix archive
RUN mix local.hex --force \ 
&& mix local.rebar --force \ 
&& mix archive.install --force https://github.com/phoenixframework/archives/raw/master/phx_new.ez

# install Node.js (>= 6.0.0) and NPM in order to satisfy brunch.io dependencies
RUN curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash - \ 
&& sudo apt-get update && sudo apt-get install -y nodejs \ 
# clean up after ourselves
&& apt-get clean

WORKDIR /code

CMD ["/bin/bash"]